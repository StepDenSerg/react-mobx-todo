import {action, computed, observable} from "mobx";
import {Task} from "../classes/tasks.classes";

export type Filter = 0 | 1 | 2;

export class AppStore {
    @observable
    private tasksList: Task[] = [new Task('Task 1'), new Task('Task 2')];

    @observable
    private historyIndex: number = 0;

    @observable
    public filter: Filter = 0;

    @computed
    public get visibleTasks(): Task[] {
        return this.filterTasks(this.history[this.historyIndex]);
    }

    @computed
    public get blockUndo(): boolean {
        return !this.historyIndex;
    }

    @computed
    public get blockRedo(): boolean {
        return this.history.length === (this.historyIndex + 1);
    }

    private history: Task[][] = [[...this.tasksList]];

    constructor() {
    }

    @action
    private updateHistory(newTasksList: Task[]) {
        this.history = this.history.slice(0, this.historyIndex + 1);
        this.history = [...this.history, newTasksList];
        this.historyIndex = this.history.length - 1;
    }

    @action
    public setFilter(n: Filter) {
        this.filter = n;
    }

    @action
    public undo = () => {
        this.historyIndex -= 1;
    };

    @action
    public redo = () => {
        this.historyIndex += 1;
    };

    public addTask(text: string) {
        const newTasksList = [...this.history[this.historyIndex]];
        newTasksList.push(new Task(text));
        this.updateHistory(newTasksList);
    }

    public removeTask(id: number) {
        const newTasksList = [...this.history[this.historyIndex]];
        const index = newTasksList.findIndex(task => task.id === id);
        newTasksList.splice(index, 1);
        this.updateHistory(newTasksList);
    }

    public checkTask(id: number) {
        let taskIndex = 0;
        const newTasksList = [...this.history[this.historyIndex]];
        const currentTask = newTasksList.find((task: Task, index) => {
            taskIndex = index;
            return task.id === id;
        });

        if (currentTask) {
            const newTask: Task = {
                ...currentTask,
                isCompleted: !currentTask.isCompleted
            };

            newTasksList.splice(taskIndex, 1, newTask);
            this.updateHistory(newTasksList);
        }
    }

    private filterTasks(tasks: Task[]) {
        if (this.filter === 1) {
            return tasks.filter(task => task.isCompleted);
        } else if (this.filter === 2) {
            return tasks.filter(task => !task.isCompleted);
        } else {
            return tasks;
        }
    }

}

export const appStore = new AppStore();
