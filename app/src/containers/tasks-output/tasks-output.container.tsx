import React, {Component} from 'react';
import classes from './tasks-output.container.module.scss';
import {AppStore} from "../../stores/app.store";
import {observer} from "mobx-react";
import TaskComponent from "./task/task.component";

interface TasksInputProps {
    store: AppStore,
}

@observer
class TasksOutputContainer extends Component<TasksInputProps> {
    checkTask = (id: number) => {
        this.props.store.checkTask(id);
    };

    removeTask = (id: number) => {
        this.props.store.removeTask(id);
    };

    render() {
        const tasksTemplate = this.props.store.visibleTasks.map(task => (
            <TaskComponent
                key={task.id}
                task={task}
                check={this.checkTask}
                remove={this.removeTask}
            />
        ));

        return (
            <div className={classes.TasksOutput}>
                <div>
                    {this.props.store.visibleTasks.length ?
                        tasksTemplate :
                        <span>List is Empty!</span>}
                </div>
            </div>
        );
    }
}

export default TasksOutputContainer;
