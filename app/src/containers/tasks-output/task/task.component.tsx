import React from 'react';
import {Task} from "../../../classes/tasks.classes";
import Button from "../../../components/button/button.component";
import classes from './task.component.module.scss'

interface TaskProps {
    task: Task,
    remove: (n: number) => void,
    check: (n: number) => void
}

const TaskComponent = (props: TaskProps) => {
    const handleCheck = () => {
        props.check(props.task.id);
    };

    const handleRemove = () => {
        props.remove(props.task.id);
    };

    return (
        <div className={classes.Task}>
            <input
                className={classes.Checkbox}
                type="checkbox"
                checked={props.task.isCompleted}
                onChange={handleCheck}
            />
            <div onClick={handleCheck} className={classes.Text}>{props.task.text}</div>
            <Button remove={true} clicked={handleRemove}>
                Remove
            </Button>
        </div>
    );
};

export default React.memo(TaskComponent);
