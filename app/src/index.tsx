import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import AppComponent from "./App.component";

ReactDOM.render(<AppComponent />, document.getElementById('root'));

serviceWorker.register();
