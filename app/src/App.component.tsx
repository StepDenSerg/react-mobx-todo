import React, { Component } from 'react';
import HeaderComponent from "./components/header/header.component";
import AppContent from "./components/app-content/app-content.component";
import DevTools from "mobx-react-devtools";

class AppComponent extends Component {
  render() {
    return (
      <div className="App">
        <HeaderComponent />
        <AppContent />
        <DevTools />
      </div>
    );
  }
}

export default AppComponent;
