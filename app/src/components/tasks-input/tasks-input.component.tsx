import React, {useState} from 'react';
import classes from './tasks-input.component.module.scss';
import {AppStore} from "../../stores/app.store";

interface TasksInputDispatchProps {
    store: AppStore,
}

const TasksInput = (props: TasksInputDispatchProps) => {
    const [inputValue, updateInputValue] = useState('');

    const addTask = () => {
        if (inputValue) {
            updateInputValue('');
            props.store.addTask(inputValue);
        }
    };

    return (
        <div className={classes.TasksInputContainer}>
            <form
                onSubmit={(e) => {e.preventDefault(); addTask()}}
            >
                <input
                    placeholder={'Enter new Task'}
                    className={classes.InputField}
                    value={inputValue}
                    onChange={(e) => updateInputValue(e.target.value)}
                    type="text"
                />
            </form>
        </div>
    )
};

export default TasksInput;
