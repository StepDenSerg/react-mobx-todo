import React from 'react';
import classes from './app-content.component.module.scss';
import TasksOutputContainer from "../../containers/tasks-output/tasks-output.container";
import FiltersBar from "../filters-bar/filters-bar.component";
import {appStore} from "../../stores/app.store";
import TasksInput from "../tasks-input/tasks-input.component";


export const AppContent = () => (
    <div className={classes.AppContent}>
        <TasksInput store={appStore}/>
        <FiltersBar store={appStore}/>
        <TasksOutputContainer store={appStore}/>
    </div>
)

export default AppContent;
