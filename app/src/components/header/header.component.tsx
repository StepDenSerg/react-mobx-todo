import React from 'react';
import classes from './header.component.module.scss';

export const HeaderComponent = () => {
    return (
        <div className={classes.header}>Naya. ToDo App.</div>
    );
};

export default React.memo(HeaderComponent);
