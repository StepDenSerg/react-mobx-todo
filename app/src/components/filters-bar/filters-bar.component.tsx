import React from 'react';
import classes from "./filters-bar.component.module.scss";
import HistoryButtons from "../history-buttons/history-buttons.component";
import Button from "../button/button.component";
import {AppStore} from "../../stores/app.store";
import {observer} from "mobx-react";

interface FiltersBarProps {
    store: AppStore
}

const FiltersBar = observer((props: FiltersBarProps) => {
    return (
        <div className={classes.FiltersWrapper}>
            <div className={classes.FiltersBar}>
                <div>
                    <Button filter={true} active={props.store.filter === 0}
                            clicked={() => props.store.setFilter(0)}>All</Button>
                    <Button filter={true} active={props.store.filter === 1}
                            clicked={() => props.store.setFilter(1)}>Completed</Button>
                    <Button filter={true} active={props.store.filter === 2}
                            clicked={() => props.store.setFilter(2)}>Active</Button>
                </div>
                <div>
                    <HistoryButtons store={props.store}/>
                </div>
            </div>
        </div>
    );
});

export default FiltersBar;
