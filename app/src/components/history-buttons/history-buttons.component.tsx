import Button from "../button/button.component";
import React from "react";
import {AppStore} from "../../stores/app.store";
import {observer} from "mobx-react";

interface HistoryButtonsProps {
   store: AppStore
}

const HistoryButtons = observer((props: HistoryButtonsProps) => (
        <React.Fragment>
            <Button disabled={props.store.blockUndo} clicked={props.store.undo}>
                <i className="fas fa-undo"></i>
            </Button>
            <Button disabled={props.store.blockRedo} clicked={props.store.redo}>
                <i className="fas fa-redo"></i>
            </Button>
        </React.Fragment>
));

export default HistoryButtons;
